import 'dart:convert';

import 'package:FlutterCourseProject/Models/MenuItem.dart';
import 'package:FlutterCourseProject/Models/Restaurant.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class MainProvider extends ChangeNotifier {
  static int selectedRestaurant = -1;

  static void selectRestaurant(BuildContext context, int restID) {
    print("Changed selected Restaurant ID to " + restID.toString());
    selectedRestaurant = restID;
  }

  List<Restaurant> _restaurants = [];
  List<MenuItem> _menuItems = [];

  List<MenuItem> get menuItems => _menuItems;
  List<Restaurant> get restaurants => _restaurants;

  Restaurant findRestaurant(int id) =>
      restaurants.firstWhere((restaurant) => restaurant.id == id);

  void _setRestaurants(List<Restaurant> restaurants) {
    _restaurants = restaurants;
  }

  void _setMenuItems(List<MenuItem> menuItems) {
    _menuItems = menuItems;
  }

  void initalLoad() {
    fetchRestaurantsData();
  }

  Future<bool> fetchRestaurantsData() async {
    const url = 'http://appback.ppu.edu/restaurants';
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as List;
      // if (extractedData['error'] != null || extractedData == null) return false;

      List<Restaurant> newRestaurants =
          extractedData.map((e) => Restaurant.fromJson(e)).toList();
      _setRestaurants(newRestaurants);
      // _setRestaurants(extractedData.map((e) => Restaurant.fromJson(e)).toList());
      print('FETCHING RESTAURANTS DATA SUCCESS!');
      return true;
    } catch (e) {
      print('ERROR FETCHING DATA !');
      return false;
    }
  }

  Future<bool> fetchMenuItemData() async {
    if (selectedRestaurant == -1) return false;
    String url =
        'http://appback.ppu.edu/menus/${selectedRestaurant.toString()}';

    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as List;
      // if (extractedData['error'] != null || extractedData == null) return false;
      List<MenuItem> newMenuItems =
          extractedData.map((e) => MenuItem.fromJson(e)).toList();
      _setMenuItems(newMenuItems);
      print(extractedData);
      // _setRestaurants(extractedData.map((e) => Restaurant.fromJson(e)).toList());
      print('FETCHING MENUITEMS DATA SUCCESS!');
      return true;
    } catch (error) {
      print(error.toString());
      Fluttertoast.cancel();
      Fluttertoast.showToast(
          msg: "تحقق من اتصالك",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red[100],
          textColor: Colors.black,
          fontSize: 16.0);

      return false;
    }
  }

  Future<bool> fetchOrderListData() async {
    if (selectedRestaurant == -1) return false;
    String url =
        'http://appback.ppu.edu/menus/${selectedRestaurant.toString()}';

    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as List;
      // if (extractedData['error'] != null || extractedData == null) return false;

      List<MenuItem> newMenuItems =
          extractedData.map((e) => MenuItem.fromJson(e)).toList();
      _setMenuItems(newMenuItems);
      print(extractedData);
      // _setRestaurants(extractedData.map((e) => Restaurant.fromJson(e)).toList());
      print('FETCHING MENUITEMS DATA SUCCESS!');
      return true;
    } catch (error) {
      Fluttertoast.cancel();
      Fluttertoast.showToast(
          msg: "تحقق من اتصالك",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red[100],
          textColor: Colors.black,
          fontSize: 16.0);
      return false;
    }
  }

  static Future<List<dynamic>> getOrderList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<dynamic> orderList = [];
    return (prefs.getString("orderList") == "")
        ? orderList
        : orderList = jsonDecode(prefs.getString("orderList"));
  }

  static void removeFromOrderList(int menuItemId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<dynamic> orderList = [];

    // List is there,
    print("Got the prefs for orderList :" + prefs.getString("orderList"));
    if (prefs.getString("orderList") == "") {
      return;
    }
    orderList = jsonDecode(prefs.getString("orderList"));
    if (orderList.contains(menuItemId)) {
      print("Found item in list !");
      orderList.remove(menuItemId);
      String newJsonArray = jsonEncode(orderList);
      prefs.setString("orderList", newJsonArray);
      print("removed item from the list");
    }
  }

  static void addToOrderList(int menuItemId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<dynamic> orderList = [];

    if (!(prefs.containsKey("orderList")) ||
        (prefs.getString("orderList")) == "") {
      // First Creation
      print("Created Prefs for orderList");
      prefs.setString("orderList", "[]");
      print("Value for prefs :" + prefs.getString("orderList"));
    } else {
      // List is there,
      print("Got the prefs for orderList :" + prefs.getString("orderList"));
      orderList = jsonDecode(prefs.getString("orderList"));
      if (orderList.contains(menuItemId)) {
        //Already in the list
        print("Already in list !");
      } else {
        orderList.add(menuItemId);
        String newJsonArray = jsonEncode(orderList);
        prefs.setString("orderList", newJsonArray);
        print(newJsonArray);
      }
    }
  }
}

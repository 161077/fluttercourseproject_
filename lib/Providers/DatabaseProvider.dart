import 'package:FlutterCourseProject/Models/MenuItem.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  DatabaseProvider._();

  static final DatabaseProvider db = DatabaseProvider._();
  static final int version = 1;
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      print("database Already Exists");
      return _database;
    }
    _database = await initDB();
    return _database;
  }

  Future<Database> initDB() async {
    String path = await getDatabasesPath();
    path += 'menuItemsTable.db';

    // print("Dropping DATABASE !");
    // Database db = await openDatabase(path,version: version, onCreate: (x, y) async {});
    // await db.execute("DROP TABLE IF EXISTS menuItems");

    print("Initiating Database...");
    return await openDatabase(
      path,
      version: version,
      onCreate: (db, version) async {
        print('Creating the database for the first time!');
        await db
            .execute('''
          create table menuItemsTable (
            id integer primary key autoincrement,
            restId integer ,
            rating integer ,
            name text not null,
            image text not null,
            description text not null,
            price integer not null
          )
          ''')
            .then((value) => print("Finished Initiating ORDERED Database..."))
            .catchError((e) => print('Error Creating ORDERED Database...'));
        await db
            .execute('''
          create table favoritesTable (
            id integer primary key autoincrement,
            restId integer ,
            rating integer ,
            name text not null,
            image text not null,
            description text not null,
            price integer not null
          )
          ''')
            .then((value) => print("Finished Initiating FAVORITES Database..."))
            .catchError((e) => print('Error Creating FAVORITES Database...'));
      },
    );
  }

  Future<List<MenuItem>> get menuItems async {
    final db = await database;
    List<Map> result = await db.query('menuItemsTable', orderBy: 'id asc');
    List<MenuItem> prds = [];
    for (var value in result) {
      prds.add(MenuItem.fromMap(value));
    }
    return prds;
  }
  Future<List<MenuItem>> get menuItemsFav async {
    final db = await database;
    List<Map> result = await db.query('favoritesTable', orderBy: 'id asc');
    List<MenuItem> prds = [];
    for (var value in result) {
      prds.add(MenuItem.fromMap(value));
    }
    return prds;
  }

  Future insert(MenuItem menuItem) async {
    final dbz = await database;
    MenuItem queryItem = await getMenuItem(menuItem.id);
    if (queryItem == null) {
      return await dbz.insert('menuItemsTable', menuItem.toMap());
    } else {
      print('ITEM ALREADY EXISTS !');
    }
//    return await db.rawInsert('''insert into menuItems (pName,quantity,price)
//                  values (?,?,?)'''
//        ,[menuItem.menuItemName,menuItem.quantity,menuItem.price]);
  }

  Future insertFav(MenuItem menuItem) async {
    final dbz = await database;
    MenuItem queryItem = await getMenuItemFav(menuItem.id);
    if (queryItem == null) {
      return await dbz.insert('favoritesTable', menuItem.toMap());
    } else {
      print('ITEM ALREADY EXISTS !');
    }
//    return await db.rawInsert('''insert into menuItems (pName,quantity,price)
//                  values (?,?,?)'''
//        ,[menuItem.menuItemName,menuItem.quantity,menuItem.price]);
  }

  Future<MenuItem> getMenuItem(int id) async {
    print("Searching for item in database!");
    final db = await database;
    List<Map> menuItems =
        await db.query('menuItemsTable', where: 'id=?', whereArgs: [id]);
    if (menuItems.isEmpty) {
      print("Item not found!");
      return null;
    } else {
      print("Item found!");
      return MenuItem.fromMap(menuItems[0]);
    }
  }

  Future<MenuItem> getMenuItemFav(int id) async {
    print("Searching for item in database!");
    final db = await database;
    List<Map> menuItems =
        await db.query('favoritesTable', where: 'id=?', whereArgs: [id]);
    if (menuItems.isEmpty) {
      print("Item not found!");
      return null;
    } else {
      print("Item found!");
      return MenuItem.fromMap(menuItems[0]);
    }
  }

  Future removeAll() async {
    final db = await database;
    return await db.delete('menuItemsTable');
  }

  Future removeAllFav() async {
    final db = await database;
    return await db.delete('menuItemsTable');
  }

  Future<int> removeMenuItem(int id) async {
    final db = await database;
    return await db.delete('menuItemsTable', where: 'id=?', whereArgs: [id]);
  }

  Future<int> removeMenuItemFav(int id) async {
    final db = await database;
    return await db.delete('favoritesTable', where: 'id=?', whereArgs: [id]);
  }

  Future<int> updateMenuItem(MenuItem menuItem) async {
    final db = await database;
    return await db.update('menuItemsTable', menuItem.toMap(),
        where: 'id=?', whereArgs: [menuItem.id]);
  }

  Future<int> updateMenuItemFav(MenuItem menuItem) async {
    final db = await database;
    return await db.update('favoritesTable', menuItem.toMap(),
        where: 'id=?', whereArgs: [menuItem.id]);
  }

  Future<int> dtopMenuItemsTable(MenuItem menuItem) async {
    final db = await database;
    return await db.update('menuItemsTable', menuItem.toMap(),
        where: 'id=?', whereArgs: [menuItem.id]);
  }

  Future<int> dtopMenuItemsTableFav(MenuItem menuItem) async {
    final db = await database;
    return await db.update('favoritesTable', menuItem.toMap(),
        where: 'id=?', whereArgs: [menuItem.id]);
  }
}

import 'package:FlutterCourseProject/Providers/DatabaseProvider.dart';
import 'package:FlutterCourseProject/Providers/MainProvider.dart';
import 'package:FlutterCourseProject/Screens/FavoritesPage.dart';
import 'package:FlutterCourseProject/Screens/MenuItemsPAge.dart';
import 'package:FlutterCourseProject/Screens/OrderListPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Screens/HomePage.dart';

void main() {
  runApp(MyApp());
  DatabaseProvider.db.database;
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MainProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          appBarTheme: AppBarTheme(backgroundColor: Colors.deepPurpleAccent),
          primaryColor: Colors.deepPurple,
          accentColor: Colors.amber,
        ),
        routes: {
          HomePage.routeName: (ctx) => HomePage(),
          MenuItemsPage.routeName: (ctx) => MenuItemsPage(),
          OrderListPage.routeName: (ctx) => OrderListPage(),
          FavoriteListPage.routeName: (ctx) => FavoriteListPage(),
        },
        home: HomePage(title: 'Talabat App'),
      ),
    );
  }
}

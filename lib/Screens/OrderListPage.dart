import 'dart:convert';

import 'package:FlutterCourseProject/Models/MenuItem.dart';
import 'package:FlutterCourseProject/Providers/DatabaseProvider.dart';
import 'package:FlutterCourseProject/Providers/MainProvider.dart';
import 'package:FlutterCourseProject/Widgets/MenuItemCard.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderListPage extends StatefulWidget {
  static const routeName = '/order_list_page';
  final String title;

  OrderListPage({Key key, this.title}) : super(key: key);
  @override
  _OrderListPageState createState() => _OrderListPageState();
}

List<MenuItem> orderedItems;

class _OrderListPageState extends State<OrderListPage> {
  MainProvider dataProvider;
  @override
  void initState() {
    super.initState();
    dataProvider = context.read<MainProvider>();
    // context.read<MainProvider>().initalLoad();
  }

  void refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      drawerScrimColor: Theme.of(context).accentColor.withAlpha(50),
      appBar: AppBar(
        title: Text(
          'Order List',
          style: TextStyle(color: Colors.black87),
        ),
        iconTheme: IconThemeData(color: Colors.black87),
        backgroundColor: Theme.of(context).primaryColorLight,
      ),
      // endDrawer: MainpageDrawer(),
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Text(
                "List of the Order:",
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
            FutureBuilder(
                future:
                    // context.watch<MainProvider>().fetchRestaurantsData(),
                    orderedData(),
                builder: (context, snapshot) {
                  print(snapshot.connectionState);
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  } else if (snapshot.connectionState == ConnectionState.done)
                    return SingleChildScrollView(
                      child: Container(
                        height: screenHeight * 0.75,
                        child: ListView.builder(
                          itemCount: orderedItems.length,
                          itemBuilder: (BuildContext context, int index) {
                            return MenuItemsCard(
                              menuItem: orderedItems[index],
                            );
                          },
                        ),
                      ),
                    );
                  return Text("Check your Connection !");
                })
          ],
        ),
      ),
    );
  }

  Future<dynamic> orderedData() async {
    orderedItems = await DatabaseProvider.db.menuItems;
  }
}

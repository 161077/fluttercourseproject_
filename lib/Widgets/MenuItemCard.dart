import 'dart:convert';

import 'package:FlutterCourseProject/Models/MenuItem.dart';
import 'package:FlutterCourseProject/Providers/DatabaseProvider.dart';
import 'package:FlutterCourseProject/Providers/MainProvider.dart';
import 'package:FlutterCourseProject/Screens/MenuItemsPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:provider/provider.dart';

class MenuItemsCard extends StatefulWidget {
  final MenuItem menuItem;

  const MenuItemsCard({
    Key key,
    this.menuItem,
  }) : super(key: key);

  @override
  _MenuItemsCardState createState() => _MenuItemsCardState();
}

class _MenuItemsCardState extends State<MenuItemsCard> {
  bool isOrdered = false;
  bool isFavourite = false;
  @override
  void initState() {
    checkInfo();
    super.initState();
  }

  checkInfo() async {
    List<MenuItem> orderList = await DatabaseProvider.db.menuItems;
    isOrdered = false;
    orderList.forEach((element) {
      if (element.id == widget.menuItem.id) isOrdered = true;
    });
    List<MenuItem> favoriteList = await DatabaseProvider.db.menuItemsFav;
    isFavourite = false;
    favoriteList.forEach((element) {
      if (element.id == widget.menuItem.id) isFavourite = true;
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.all(10),
      child: Card(
        clipBehavior: Clip.antiAlias,
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        child: Container(
          width: double.infinity,
          height: screenHeight * 0.6,
          // padding: EdgeInsets.all(20),
          child: InkWell(
            splashColor: Theme.of(context).accentColor,
            onTap: () {}, //onPressedCard(context),
            child: Column(
              children: [
                FadeInImage(
                  placeholder: AssetImage('assets/images/placeholder.png'),
                  image: NetworkImage('http://appback.ppu.edu/static/' +
                      this.widget.menuItem.image),
                  width: double.maxFinite,
                  fit: BoxFit.cover,
                  height: screenHeight * 0.35,
                  imageErrorBuilder: (context, error, stackTrace) => Container(
                    color: Colors.red[100],
                    alignment: Alignment.center,
                    height: screenHeight * 0.35,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Failed to load image !',
                          style: TextStyle(fontSize: 30),
                        ),
                        Icon(
                          Icons.error_outline,
                          color: Colors.red[800],
                          size: 40,
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Icon(Icons.assignment),
                                SizedBox(width: 10),
                                Text(
                                  this.widget.menuItem.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  this.widget.menuItem.price.toString(),
                                  style: TextStyle(
                                      color: Colors.grey[800], fontSize: 25),
                                ),
                                SizedBox(width: 5),
                                Icon(
                                  Icons.attach_money,
                                  size: 40,
                                  color: Colors.grey[800],
                                ),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            !isOrdered
                                ? FlatButton(
                                    onPressed:
                                        _addToOrderList, // onPressedCard(context),
                                    child: Row(
                                      children: [
                                        Icon(Icons.add_shopping_cart),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text('Order',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6
                                                .copyWith(
                                                    color: Colors.black87)),
                                      ],
                                    ),
                                    color: Colors.green[400],
                                  )
                                : Row(
                                    children: [
                                      IconButton(
                                          icon: Icon(
                                            Icons.remove_circle_outline,
                                            color: Colors.red[300],
                                          ),
                                          onPressed: _removeFromOrderList),
                                      Text(
                                        'In OrderList',
                                        style:
                                            TextStyle(color: Colors.green[400]),
                                      ),
                                    ],
                                  ),
                            !isFavourite
                                ? FlatButton(
                                    onPressed:
                                        _addToFavourites, // onPressedCard(context),
                                    child: Row(
                                      children: [
                                        Icon(Icons.favorite),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text('Add to favourites',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2),
                                      ],
                                    ),
                                    color: Theme.of(context).accentColor,
                                  )
                                : Row(
                                    children: [
                                      Text('already in favorites !'),
                                      IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: _removeFromFavorites,
                                        iconSize: 30,
                                      ),
                                    ],
                                  ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Center(
                              child: RatingBar.builder(
                                initialRating: this.widget.menuItem.rating / 2,
                                minRating: 1,
                                ignoreGestures: true,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 4.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber[300],
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                            ),
                            FlatButton(
                              onPressed: () {},
                              child: Text("Rate !"),
                              color: Theme.of(context).primaryColorLight,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _addToOrderList() async {
    await DatabaseProvider.db
        .insert(widget.menuItem)
        .then((value) => checkInfo());
    // MainProvider.addToOrderList(this.widget.menuItem.id);
  }

  _removeFromOrderList() async {
    DatabaseProvider.db.removeMenuItem(widget.menuItem.id);
    // MainProvider.removeFromOrderList(this.widget.id);
    checkInfo();
  }

  _removeFromFavorites() async {
    DatabaseProvider.db.removeMenuItemFav(widget.menuItem.id);
    // MainProvider.removeFromOrderList(this.widget.id);
    checkInfo();
  }

  _addToFavourites() async {
    await DatabaseProvider.db
        .insertFav(widget.menuItem)
        .then((value) => checkInfo());
    // MainProvider.addToOrderList(this.widget.menuItem.id);
  }
}
